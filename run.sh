#!/bin/bash

if [ "$1" == "prod" ]
then
    echo "Running as prod"
    cp /code/app/config/parameters_prod.yml.dist /code/app/config/parameters.yml
    APP_ENVIRONMENT="prod"
    MYSQL_HOST="172.17.42.1"
else
    APP_ENVIRONMENT="dev"
    MYSQL_HOST="mysql"
fi

rm app/cache/* app/cache/* -Rf
php app/console cache:clear --env=${APP_ENVIRONMENT}
chmod 777 app/cache app/logs -R

# Load mysql tables
USERNAME=phpday
PASSWORD=phpday
DATABASE=phpday
exit_code=1
while [ $exit_code -ne 0 ]; do
    mysql -h ${MYSQL_HOST} -u ${USERNAME} --password=${PASSWORD} ${DATABASE} -e "select version()" >/dev/null
    exit_code=$?
    [ $exit_code -ne 0 ] && (echo "Trying to connect"; sleep 1)
done

mysql -h ${MYSQL_HOST} -u ${USERNAME} --password=${PASSWORD} ${DATABASE} < /code/ansible/tables.sql

echo "Tables were created"

# Run apache
/usr/sbin/apache2ctl -D FOREGROUND
